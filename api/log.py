import logging.config
import structlog


def configure_logging():
    logging.config.dictConfig({
        "version": 1,
        "disable_existing_loggers": True,
        "formatters": {
            "structlog": {
                "()": structlog.stdlib.ProcessorFormatter,
                "processor": structlog.dev.ConsoleRenderer(colors=False),
            },
            "plain": {
                "format": '%(message)s'
            }
        },
        "handlers": {
            "default": {
                "level": "DEBUG",
                "class": "logging.StreamHandler",
                "formatter": "plain",
            },
            "structlog": {
                "level": "DEBUG",
                "class": "logging.StreamHandler",
                "formatter": "structlog",
                "stream": "ext://sys.stdout"
            }
        },
        "loggers": {
            "aiohttp.access": {
                "handlers": ["default"],
                "level": "DEBUG",
            },
            "spaceman_api": {
                "handlers": ["structlog"],
                "level": "DEBUG",
            }
        }
    })
    structlog.configure(
        processors=[
            structlog.stdlib.filter_by_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.TimeStamper(fmt='iso'),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.processors.JSONRenderer(),
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        context_class=dict,
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
        cache_logger_on_first_use=True,
    )
