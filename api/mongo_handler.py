from pymongo import MongoClient
from pymongo.errors import OperationFailure
from contextlib import closing
from bson.json_util import dumps, ObjectId


def list_planets():
    """Return all the planets saved on the database."""
    with closing(MongoClient()) as client:
        db = client["planets"]
        collection = list(db.planets.find())
        return dumps(collection)


def search_by_name(name):
    """Search for a plane by name and return the first one."""
    with closing(MongoClient()) as client:
        db = client["planets"]
        planet = db.planets.find_one({"name": name})
        if planet:
            return dumps(planet)
        else:
            return None


def search_by_id(id):
    """Search for a planet by id and return the first one."""
    with closing(MongoClient()) as client:
        db = client["planets"]
        planet = db.planets.find_one({"_id": ObjectId(id)})
        if planet:
            return dumps(planet)
        else:
            return None


def delete_by_name(name):
    """Delete a planet from the collection by name."""
    with closing(MongoClient()) as client:
        db = client["planets"]
        result = db.planets.delete_one({"name": name})
        if result.deleted_count != 1:
            raise OperationFailure


def delete_by_id(id):
    """Delete a planet from the collection by id."""
    with closing(MongoClient()) as client:
        db = client["planets"]
        result = db.planets.delete_one({"_id": ObjectId(id)})
        if result.deleted_count != 1:
            raise OperationFailure()


def insert(data):
    """Insert a new element to the database."""
    with closing(MongoClient()) as client:
        db = client["planets"]
        planet = {
            'name': data['name'],
            'climate': data['climate'],
            'terrain': data['terrain']
        }
        db.planets.insert_one(planet)
