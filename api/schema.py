from marshmallow import Schema, fields


class InsertSchema(Schema):
    """Schema to validate insertion data."""

    name = fields.Str(required=True)
    terrain = fields.Str(required=True)
    climate = fields.Str(required=True)
