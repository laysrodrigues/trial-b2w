import re
import json
import structlog
import argparse
from aiohttp import web
from . import mongo_handler
from .schema import InsertSchema
from .log import configure_logging
from marshmallow import ValidationError
from pymongo.errors import OperationFailure

planet_schema = InsertSchema(strict=True)


async def list_all_planets(request):
    """Return a json with all planets on the db."""
    try:
        return web.json_response(mongo_handler.list_planets())
    except OperationFailure as e:
        request.app['logger'].exception(e)
        return web.HTTPInternalServerError()


async def search(request):
    """Search a planet by ID or Name."""
    param = request.match_info['param']
    logger = request.app['logger']
    if is_regex(param):
        try:
            result = mongo_handler.search_by_id(param)
            if result:
                return web.json_response(result)
            else:
                logger.debug('Id not found: ', planet=param)
                return web.HTTPNotFound()
        except OperationFailure as e:
            logger.exception(e)
            return web.HTTPInternalServerError()
    else:
        try:
            result = mongo_handler.search_by_name(param)
            if result:
                return web.json_response(result)
            else:
                logger.debug('Name not found: ', planet=param)
                return web.HTTPNotFound()
        except OperationFailure as e:
            logger.exception(e)
            return web.HTTPInternalServerError()


async def delete(request):
    """Delete a planet by ID or Name."""
    logger = request.app['logger']
    param = request.match_info['param']
    if is_regex(param):
        try:
            mongo_handler.delete_by_id(param)
        except OperationFailure as e:
            logger.exception(e)
            return web.HTTPInternalServerError()
    else:
        try:
            mongo_handler.delete_by_name(param)
        except OperationFailure as e:
            logger.exception(e)
            return web.HTTPInternalServerError()
    return web.HTTPOk()


async def insert(request):
    """Insert a new element to the database."""
    logger = request.app['logger']
    try:
        j = await request.json()
        data = planet_schema.load(j).data
        mongo_handler.insert(data)
        return web.HTTPOk()
    except json.JSONDecodeError as e:
        logger.exception(e)
        return web.HTTPBadRequest()
    except ValidationError as e:
        logger.exception(e)
        return web.json_response({'error': e.messages}, status=400)
    except OperationFailure as e:
        logger.exception(e)
        return web.HTTPInternalServerError()


async def healthcheck(request):
    """"Return if the api is up."""
    return web.HTTPOk()


def is_regex(input):
    """Use regex to validate if is a ID or a Name."""
    regex = re.compile("([a-f\d]{24})")
    if regex.match(input):
        return True
    else:
        return False


def create_app():
    """Setup the application."""
    configure_logging()
    app = web.Application()
    app['logger'] = structlog.get_logger("spaceman_api")
    add_routes(app)
    return app


def add_routes(app):
    """Add the endpoints of this application."""
    app.router.add_get('/v1/list', list_all_planets)
    app.router.add_get('/v1/search/{param}', search)
    app.router.add_get('/v1/delete/{param}', delete)
    app.router.add_post('/v1/insert', insert)
    app.router.add_get('/v1/healthcheck', healthcheck)


def main():
    parser = argparse.ArgumentParser(description="Aiohttp setup")
    parser.add_argument('--port', type=int)
    args = parser.parse_args()
    app = create_app()
    if args:
        web.run_app(app, **vars(args))
    else:
        web.run_app(app)
