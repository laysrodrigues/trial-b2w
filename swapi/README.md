#### Importing a Collection to Mongo

To import a collection to MongoDB use the
following command:

```
mongoimport --db planets --collection planets --file planets.json
```

If you don't set ```--collection``` the collection will have the name
of the input file.
