from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from unittest.mock import patch
from api.main import create_app
from pymongo.errors import OperationFailure


class TestView(AioHTTPTestCase):
    """Test all routes of the application."""

    async def get_application(self):
        """Create the test application."""
        app = create_app()
        return app

    @unittest_run_loop
    async def test_get_planets(self):
        """Test get all planets."""
        with patch('api.mongo_handler.list_planets') as mocked_method:
            mocked_method.return_value = "{}"
            response = await self.client.get("/v1/list")
            assert response.status == 200
            mocked_method.assert_called_once_with()

    @unittest_run_loop
    async def test_get_planets_raise_operation_failure(self):
        """Test get all planets."""
        with patch('api.mongo_handler.list_planets') as mocked_method:
            mocked_method.side_effect = OperationFailure
            response = await self.client.get("/v1/list")
            assert response.status == 500
            mocked_method.assert_called_once_with()

    @unittest_run_loop
    async def test_search_planet_by_id(self):
        """Test search for a planet by id."""
        with patch('api.mongo_handler.search_by_id') as mocked_method:
            mocked_method.return_value = "{}"
            object_id = '5a844792a79fb4e63bd3789c'
            response = await self.client.get("/v1/search/" + object_id)
            assert response.status == 200
            assert await response.json() == "{}"
            mocked_method.assert_called_once_with(object_id)

    @unittest_run_loop
    async def test_search_planet_by_id_not_found(self):
        """Test search for a planet by id not found."""
        with patch('api.mongo_handler.search_by_id') as mocked_method:
            mocked_method.return_value = None
            object_id = '5a844792a79fb4e63bd3789c'
            response = await self.client.get("/v1/search/" + object_id)
            assert response.status == 404
            mocked_method.assert_called_once_with(object_id)

    @unittest_run_loop
    async def test_search_planet_by_id_operation_error(self):
        """Test search for a planet by id operation error."""
        with patch('api.mongo_handler.search_by_id') as mocked_method:
            mocked_method.side_effect = OperationFailure
            object_id = '5a844792a79fb4e63bd3789c'
            response = await self.client.get("/v1/search/" + object_id)
            assert response.status == 500
            mocked_method.assert_called_once_with(object_id)

    @unittest_run_loop
    async def test_search_planet_by_name(self):
        """Test search for a planet by name."""
        with patch('api.mongo_handler.search_by_name') as mocked_method:
            mocked_method.return_value = "{}"
            name = "Alderaan"
            response = await self.client.get("/v1/search/" + name)
            assert response.status == 200
            assert await response.json() == "{}"
            mocked_method.assert_called_once_with(name)

    @unittest_run_loop
    async def test_search_planet_by_name_not_found(self):
        """Test search for a planet by name not found."""
        with patch('api.mongo_handler.search_by_name') as mocked_method:
            mocked_method.return_value = None
            name = "Alderaan"
            response = await self.client.get("/v1/search/" + name)
            assert response.status == 404
            mocked_method.assert_called_once_with(name)

    @unittest_run_loop
    async def test_search_planet_by_name_operation_error(self):
        """Test search for a planet by name operation error."""
        with patch('api.mongo_handler.search_by_name') as mocked_method:
            mocked_method.side_effect = OperationFailure
            name = "Alderaan"
            response = await self.client.get("/v1/search/" + name)
            assert response.status == 500
            mocked_method.assert_called_once_with(name)

    @unittest_run_loop
    async def test_delete_planet_by_id(self):
        """Test delete a planet by id."""
        with patch('api.mongo_handler.delete_by_id') as mocked_method:
            mocked_method.return_value = True
            object_id = '5a844792a79fb4e63bd3789c'
            response = await self.client.get("/v1/delete/" + object_id)
            assert response.status == 200
            mocked_method.assert_called_once_with(object_id)

    @unittest_run_loop
    async def test_delete_planet_by_id_operation_failure(self):
        """Test delete a planet by id operation failure."""
        with patch('api.mongo_handler.delete_by_id') as mocked_method:
            mocked_method.side_effect = OperationFailure
            object_id = '5a844792a79fb4e63bd3789c'
            response = await self.client.get("/v1/delete/" + object_id)
            assert response.status == 500
            mocked_method.assert_called_once_with(object_id)

    @unittest_run_loop
    async def test_delete_planet_by_name(self):
        """Test delete a planet by name."""
        with patch('api.mongo_handler.delete_by_name') as mocked_method:
            mocked_method.return_value = True
            name = "Alderaan"
            response = await self.client.get("/v1/delete/" + name)
            assert response.status == 200
            mocked_method.assert_called_once_with(name)

    @unittest_run_loop
    async def test_delete_planet_by_name_operation_failure(self):
        """Test delete a planet by name operation failure."""
        with patch('api.mongo_handler.delete_by_name') as mocked_method:
            mocked_method.side_effect = OperationFailure
            name = 'Alderaan'
            response = await self.client.get("/v1/delete/" + name)
            assert response.status == 500
            mocked_method.assert_called_once_with(name)

    @unittest_run_loop
    async def test_insert(self):
        """"Teste insert a new item to db."""
        with patch('api.mongo_handler.insert') as mocked_method:
            data = {"name": "Terra", "climate": "Seco", "terrain": "Arido"}
            response = await self.client.post("/v1/insert", json=data)
            assert response.status == 200
            mocked_method.assert_called_once_with(data)

    @unittest_run_loop
    async def test_insert_json_not_found(self):
        """"Teste insert raise valitation error."""
        with patch('api.mongo_handler.insert') as mocked_method:
            response = await self.client.post("/v1/insert")
            mocked_method.assert_not_called()
            assert response.status == 400

    @unittest_run_loop
    async def test_insert_raise_validation_error(self):
        """"Teste insert raise valitation error."""
        with patch('api.mongo_handler.insert') as mocked_method:
            data = {"climate": "Seco", "terrain": "Arido"}
            response = await self.client.post("/v1/insert", json=data)
            mocked_method.assert_not_called()
            assert response.status == 400

    @unittest_run_loop
    async def test_insert_raise_operation_failure(self):
        """"Teste insert raise operation failure."""
        with patch('api.mongo_handler.insert') as mocked_method:
            mocked_method.side_effect = OperationFailure
            data = {"name": "Terra", "climate": "Seco", "terrain": "Arido"}
            response = await self.client.post("/v1/insert", json=data)
            assert response.status == 500
            mocked_method.assert_called_once_with(data)
