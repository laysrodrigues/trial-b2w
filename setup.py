from setuptools import setup, find_packages

setup(
    name='SpacemanAPI',
    version='0.1',
    description='Api to consult Star Wars Planets info',
    url='https://github.com/lays147/spacemanapi',
    author='Lays Rodrigues',
    author_email='laysrodriguessilva@gmail.com',
    packages=find_packages(exclude=('tests')),
    install_requires=[
        'aiohttp==3.0.1',
        'marshmallow==2.15.0',
        'pymongo==3.6.0',
        'structlog==18.1.0',
    ],
    entry_points={
        'console_scripts': ['spacemanapi = api.main:main'],
    })
